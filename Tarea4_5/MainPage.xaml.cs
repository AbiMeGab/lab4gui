using Microsoft.UI.Xaml.Controls;
using Microsoft.UI.Xaml.Input;
using Windows.System;

namespace Tarea4_5;

public sealed partial class MainPage : Page
{
    public MainPage()
    {
        this.InitializeComponent();
        this.Loaded += MainPage_Loaded;
        Loaded += (s, e) =>
        {
            Focus(FocusState.Programmatic);
        };
    }

    private void MainPage_Loaded(object sender, RoutedEventArgs e)
    {
        if (Window.Current != null)
        {
            double windowHeight = Window.Current.Bounds.Height;
            double windowWidth = Window.Current.Bounds.Width;

            // Configuración inicial de la posición del sprite
            Canvas.SetLeft(Sprite, (windowWidth - Sprite.Width) / 2);
            Canvas.SetTop(Sprite, windowHeight - Sprite.Height);


        }
    }

    public void MainPage_KeyDown(object sender, KeyRoutedEventArgs e)
    {

        double maxWidth = GameCanvas.ActualWidth;

        double maxHeight = GameCanvas.ActualHeight;

        switch (e.Key)
        {
            case VirtualKey.Up:
                // Mueve el sprite hacia arriba
                mediaLaser.MediaPlayer.Play();
                if (Canvas.GetTop(Sprite) <= 0) return;
                Canvas.SetTop(Sprite,Canvas.GetTop(Sprite) - 5);
                break;

            case VirtualKey.Down:
                // Mueve el sprite hacia abajo
                mediaLaser.MediaPlayer.Play();
                if (Canvas.GetTop(Sprite) >= maxHeight - Sprite.Height) return;
                Canvas.SetTop(Sprite, Canvas.GetTop(Sprite) + 5);
                break;

            case VirtualKey.Left:
                // Mueve el sprite hacia la izquierda
                mediaLaser.MediaPlayer.Play();
                if (Canvas.GetLeft(Sprite) <= 0) return;
                Canvas.SetLeft(Sprite, Canvas.GetLeft(Sprite) - 5);
                break;

            case VirtualKey.Right:
                // Mueve el sprite hacia la derecha
                mediaLaser.MediaPlayer.Play();
                if (Canvas.GetLeft(Sprite) >= maxWidth - Sprite.Width) return;
                Canvas.SetLeft(Sprite, Canvas.GetLeft(Sprite) + 5);
                break;
        }

    }
}
