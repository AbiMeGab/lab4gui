using CommunityToolkit.Mvvm.ComponentModel;

namespace Tarea4_5;

public class MainViewModel : ObservableObject
{
    private double spriteX;
    private double spriteY;

    public double SpriteX
    {
        get => spriteX;
        set => SetProperty(ref spriteX, value);
    }

    public double SpriteY
    {
        get => spriteY;
        set => SetProperty(ref spriteY, value);
    }

    public MainViewModel(double windowHeight, double windowWidth, double spriteHeight, double spriteWidth)
    {
        SpriteX = (windowWidth - spriteWidth) / 2;
        SpriteY = windowHeight - spriteHeight;
    }
}
